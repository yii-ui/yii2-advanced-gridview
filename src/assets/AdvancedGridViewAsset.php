<?php

namespace yiiui\yii2advancedgridview\assets;

use yii\web\AssetBundle;

/**
 * Class AdvancedGridViewAsset
 * @package yiiui\yii2advancedgridview\assets
 */
class AdvancedGridViewAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/files';

    public $js = [
        'js/advancedgridview.js',
    ];

    public $css = [
        'css/advancedgridview.scss',
    ];

    public $depends = [
        'fafcms\assets\init\InitAsset',
    ];
}
