;(function (factory) {
  'use strict'
  new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
  'use strict'

  let newScrollBarPos
  let maxScrollBarPos

  let checkScrollBarPos = function ($actionWrapper) {
    let scrollHeight = $actionWrapper.find('.os-scrollbar.os-scrollbar-horizontal').outerHeight()
    newScrollBarPos = $(window).scrollTop() + $(window).height() - $actionWrapper.offset().top - scrollHeight
    maxScrollBarPos = $actionWrapper.height() - scrollHeight

    if (newScrollBarPos < maxScrollBarPos) {
      $actionWrapper.find('.os-scrollbar.os-scrollbar-horizontal').css('top', newScrollBarPos)
    } else {
      $actionWrapper.find('.os-scrollbar.os-scrollbar-horizontal').css('top', maxScrollBarPos)
    }
  }

  let newTableHeaderPos
  let maxTableHeaderPos
  let $pinnedItems
  let $row
  let maxTopPos
  let pinnedPos

  let checkTableHeaderPos = function ($actionWrapper) {
    maxTopPos = $('.page-header').height()
    $pinnedItems = $('.pinned:visible')

    if ($pinnedItems.length > 0) {
      $pinnedItems.each(function () {
        pinnedPos = Number.parseInt($(this).css('top').slice(0, -2)) + $(this).height()

        if (pinnedPos > maxTopPos) {
          maxTopPos = pinnedPos
        }
      })
    }

    newTableHeaderPos = $(window).scrollTop() - $actionWrapper.offset().top + maxTopPos
    maxTableHeaderPos = $actionWrapper.offset().top + $actionWrapper.height() - maxTopPos - $actionWrapper.find('thead').height() - 250

    if (newTableHeaderPos < maxTableHeaderPos) {
      $actionWrapper.find('thead tr > * > div').css('top', (newTableHeaderPos > 0 ? newTableHeaderPos : 0))
    } else {
      $actionWrapper.find('thead tr > * > div').css('top', (maxTableHeaderPos > 0 ? maxTableHeaderPos : 0))
    }
  }

  let setActionColumnSize = function ($actionWrapper) {
    let $actionColumns = $actionWrapper.find('tbody .action-column')
    let actionColumnWidth = 0
    let columnWidth = 0

    if ($actionColumns.length > 0) {
      $actionColumns.css('width', 'auto')

      $actionColumns.each(function () {
        columnWidth = $(this).width()

        if (columnWidth > actionColumnWidth) {
          actionColumnWidth = columnWidth
        }
      })

      $actionWrapper.find('.action-column').css('width', actionColumnWidth)
    }

    $actionWrapper.find('.table-scroll-wrapper').css('margin-right', 0)

    $actionWrapper.find('tr').each(function () {
      $row = $(this)
      $row.find('.action-column').height($row.height())
    })

    $actionWrapper.find('.os-viewport').css('margin-right', actionColumnWidth - 1)
  }

  let initActionWrapperTable = function ($parent) {
    $parent.find('.table-action-wrapper').each(function () {
      let $actionWrapper = $(this)

      let afterScrollbarInit = function () {
        checkScrollBarPos($actionWrapper)
        checkTableHeaderPos($actionWrapper)
      }

      $actionWrapper.on('fafcms-scrollbar-size-change', function () {
        setActionColumnSize($actionWrapper)
      })

      if ($actionWrapper.hasClass('fafcms-scrollbar-ready')) {
        setActionColumnSize($actionWrapper)
        afterScrollbarInit()
      } else {
        $actionWrapper.on('fafcms-scrollbar-ready', afterScrollbarInit)
      }
    })
  }

  fafcms.prototype.events['initPlugins'].push(function ($target) {
    initActionWrapperTable($target)
  })

  fafcms.prototype.events['scroll'].push(function () {
    $('body').find('.table-action-wrapper').each(function () {
      let $actionWrapper = $(this)

      checkScrollBarPos($actionWrapper)
      checkTableHeaderPos($actionWrapper)
    })
  })

  return fafcms
})
