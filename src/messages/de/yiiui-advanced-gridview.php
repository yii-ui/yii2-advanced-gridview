<?php
return [
    'Duplicate' => 'Duplizieren',
    'Are you sure you want to duplicate this item?' => 'Sind Sie sicher das Sie dieses Objekt duplizieren möchten?',
];
