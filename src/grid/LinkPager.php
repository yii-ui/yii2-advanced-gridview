<?php
namespace yiiui\yii2advancedgridview\grid;

class LinkPager extends \yii\widgets\LinkPager
{
    public $disabledListItemSubTagOptions = ['tag' => 'a'];
    public $linkContainerOptions = ['class' => 'waves-effect'];
    public $nextPageLabel = '<i class="mdi mdi-chevron-right"></i>';
    public $prevPageLabel = '<i class="mdi mdi-chevron-left"></i>';
}
