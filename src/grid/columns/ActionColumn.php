<?php

namespace yiiui\yii2advancedgridview\grid\columns;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $sort;
    public $template = '{create} {view} {update} {copy} {delete}';
    public $outlineIcons = true;
    public $iconClass = 'text-darken-2';
    public $buttonBackground = 'lighten-2';

    public function init()
    {
        parent::init();

        Html::addCssClass($this->filterOptions, 'action-column');
        Html::addCssClass($this->contentOptions, 'action-column');
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        $this->initDefaultButton('create', 'plus', [
            'iconOptions' => ['class' => 'primary-text '.$this->iconClass],
        ]);

        $this->initDefaultButton('view', 'magnify', [
            'iconOptions' => ['class' => 'blue-text '.$this->iconClass],
        ]);

        $this->initDefaultButton('update', 'pencil'.($this->outlineIcons?'-outline':''), [
            'iconOptions' => ['class' => 'green-text '.$this->iconClass],
        ]);

        $this->initDefaultButton('copy', 'content-duplicate', [
            'iconOptions' => ['class' => 'orange-text '.$this->iconClass],
            'data-confirm' => Yii::t('yiiui-advanced-gridview', 'Are you sure you want to duplicate this item?'),
            'data-method' => 'post',
        ]);

        $this->initDefaultButton('delete', 'delete'.($this->outlineIcons?'-outline':''), [
            'iconOptions' => ['class' => 'red-text '.$this->iconClass],
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
    }

    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'create':
                        $title = Yii::t('yii', 'Create');
                        break;
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'copy':
                        $title = Yii::t('yiiui-advanced-gridview', 'Duplicate');
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $title = ucfirst($name);
                }

                $options = $this->buttonOptions[$name]??[];

                if (isset($options['label'])) {
                    $title = $options['label'];
                    unset($options['label']);
                }

                Html::addCssClass($options, ArrayHelper::remove($additionalOptions, 'class'));
                Html::addCssClass($options, ['default' => 'btn-floating waves-effect grey '.$this->buttonBackground.' waves-circle', 'tooltipped']);

                $iconOptions = ArrayHelper::remove($additionalOptions, 'iconOptions');

                $options = array_merge([
                    'data-tooltip' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0'
                ], $additionalOptions, $options);

                Html::addCssClass($iconOptions, ['mdi', 'mdi-'.$iconName]);

                $icon = Html::tag('i', '', $iconOptions);
                return Html::a($icon, $url, $options);
            };
        }
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        return Html::tag('div', preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
            $name = $matches[1];

            if (isset($this->visibleButtons[$name])) {
                $isVisible = $this->visibleButtons[$name] instanceof \Closure
                    ? call_user_func($this->visibleButtons[$name], $model, $key, $index)
                    : $this->visibleButtons[$name];
            } else {
                $isVisible = false;
            }

            if ($isVisible && isset($this->buttons[$name])) {
                $url = $this->createUrl($name, $model, $key, $index);
                return call_user_func($this->buttons[$name], $url, $model, $key);
            }

            return '';
        }, $this->template), ['class' => 'action-column-container']);
    }
}
