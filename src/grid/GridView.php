<?php

namespace yiiui\yii2advancedgridview\grid;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\i18n\PhpMessageSource;
use yiiui\yii2advancedgridview\assets\AdvancedGridViewAsset;
use yiiui\yii2advancedgridview\grid\columns\DataColumn;
use Yii;

class GridView extends \yii\grid\GridView
{
    public $dataColumnClass = DataColumn::class;
    public $tableOptions = ['class' => 'striped highlight'];
    public $wrapperOptions = [];
    public $scrollWrapperOptions = [];

    public function init()
    {
        if (!isset(Yii::$app->i18n->translations['yiiui-advanced-gridview'])) {
            Yii::$app->i18n->translations['yiiui-advanced-gridview'] = [
                'class' => PhpMessageSource::class,
                'basePath' => dirname(__DIR__) .'/messages',
                'forceTranslation' => true,
            ];
        }

        parent::init();
    }

    public function run()
    {
        AdvancedGridViewAsset::register($this->getView());

        Html::addCssClass($this->scrollWrapperOptions, 'table-scroll-wrapper');

        $this->layout = Html::tag('div', Html::tag('div', '{items}', $this->scrollWrapperOptions), ['class' => 'table-action-wrapper fafcms-scrollbars']);
        $this->layout .= '{summary}{pager}';

        if (is_array($this->pager) && !ArrayHelper::keyExists('class', $this->pager)) {
            $this->pager = [
                'class' => LinkPager::className(),
            ];
        }

        parent::run();
    }
}
