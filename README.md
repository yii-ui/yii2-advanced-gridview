[![Yii UI](https://avatars1.githubusercontent.com/u/22790740?s=60)](https://www.yii-ui.com/) Yii2 Advanced GridView.
================================================

[![Latest Stable Version](https://poser.pugx.org/yii-ui/yii2-advanced-gridview/version)](https://packagist.org/packages/yii-ui/yii2-advanced-gridview)
[![Total Downloads](https://poser.pugx.org/yii-ui/yii2-advanced-gridview/downloads)](https://packagist.org/packages/yii-ui/yii2-advanced-gridview)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)
[![License](https://poser.pugx.org/yii-ui/yii2-advanced-gridview/license)](https://packagist.org/packages/yii-ui/yii2-advanced-gridview)


This is an [Yii framework 2.0](http://www.yiiframework.com) module for advanced grid views.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require yii-ui/yii2-advanced-gridview
```
or add
```
"yii-ui/yii2-advanced-gridview": "~1.0.0"
```
to the require section of your `composer.json` file.

Usage
-----

```php
TODO
```

More [Examples](https://www.yii-ui.com/yii2-advanced-gridview) will be added soon at https://www.yii-ui.com/yii2-advanced-gridview.

Documentation
------------

[Documentation](https://www.yii-ui.com/yii2-advanced-gridview) will be added soon at https://www.yii-ui.com/yii2-advanced-gridview.

License
-------

**yii2-advanced-gridview** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
